#!/bin/sh
PATH=/usr/kerberos/bin/:/usr/bin:$PATH
export PATH

curr_principal=`klist 2>/dev/null|egrep "Default principal" |awk '{print $3}'`
if [ "x$curr_principal" != "x" ]
then
    kinit -R
    aklog
fi
