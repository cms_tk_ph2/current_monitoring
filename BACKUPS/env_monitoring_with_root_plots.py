import time, datetime, subprocess, ROOT
from array import array

isTest = True

def getTimestamp():
	return str(time.time()).rstrip('\n')

def getVoltage():
	if isTest:
		commandV = ["echo","300"]
	else:
		commandV = ["python", "keithleyControl.py","-rv"]
	pipe = subprocess.Popen(commandV, stdout=subprocess.PIPE)
	text = pipe.communicate()
	return text[0].rstrip('\n')

def getCurrent():
	if isTest:
		commandV = ["echo","0.1"]
	else:
		commandV = ["python", "keithleyControl.py","-rc"]
	pipe = subprocess.Popen(commandV, stdout=subprocess.PIPE)
	text = pipe.communicate()
	return text[0].rstrip('\n')

def getTemperature():
	return "10"

def getHumidity():
	return "10"

def update_plots():

	environment_file = open("environment.csv",'r')
	env_list = environment_file .read().split('\n')
	n_lines = len(env_list)
	if n_lines > 1000:
		n_lines = 1000
	env_list = env_list[len(env_list)-n_lines:]

	variables = [[],[],[],[],[]]
	names = ["Voltage","Current","Temperature","Humidity",]
	graph_array = []

	for line in env_list:
		line = line.split(",")
		if len(line)>1:
			for i in range(0,len(line)):
				variables[i].append(float(line[i]))
		else:
			break

	for i in range(1,len(variables)):
		graph_array.append(ROOT.TGraph(n_lines-1, array('f', variables[0]), array('f', variables[i])))
		graph_array[i-1].SetName(names[i-1])

		c1 = ROOT.TCanvas(names[i-1], names[i-1], 800, 500)
		graph_array[i-1].SetTitle(names[i-1])
		graph_array[i-1].SetMarkerSize(1)
		graph_array[i-1].SetMarkerStyle(20)
		graph_array[i-1].SetMarkerColor(30)
		graph_array[i-1].Draw("ACP")
		graph_array[i-1].GetXaxis().SetTitle("Time")
		graph_array[i-1].GetYaxis().SetTitle(names[i-1])
		c1.Update()
		c1.SaveAs(names[i-1]+".png")
		c1.SaveAs(names[i-1]+".pdf")

	environment_file.close()
	#subprocess.call("cp *.pdf ~jluetic/www/TestBeam/.", shell=True)	
	#subprocess.call("cp *.png ~jluetic/www/TestBeam/.", shell=True)
	subprocess.call("cp *.pdf /afs/cern.ch/user/g/gauzinge/www/beamtest/monitoring", shell=True)	

if __name__=='__main__':

#def update_data_file(timestamp, temperature, humidity):

	environmentFile = open("environment.csv",'a')

	while True:

		ts = getTimestamp()
		v = getVoltage()
		i = getCurrent()
		temp = getTemperature()
		hum = getHumidity();

	
		environmentFile.write(str(ts)+","+str(v)+","+str(i)+","+str(temp)+","+str(hum)+"\n")				
		print str(ts)+","+str(v)+","+str(i)+","+str(temp)+","+str(hum)

		update_plots()

		if isTest:
			break
		else:
			time.sleep(10)

