import time, datetime, subprocess
from array import array

isTest = False

def getTimestamp():
	return str(time.time()).rstrip('\n')

def getVoltage(nr):
	if isTest:
		commandV = ["echo","300"]
	else:
		commandV = ["python", "keithleyControl.py",nr]
	pipe = subprocess.Popen(commandV, stdout=subprocess.PIPE)
	text = pipe.communicate()
	return text[0].rstrip('\n')


def getCurrent(nr):
	if isTest:
		commandV = ["echo","0.1"]
	else:
		commandV = ["python", "keithleyControl.py",nr]
	pipe = subprocess.Popen(commandV, stdout=subprocess.PIPE)
	text = pipe.communicate()
	return text[0].rstrip('\n')

if __name__=='__main__':

#def update_data_file(timestamp, temperature, humidity):

	currentFile = open("currents.csv",'a')
	currentFile.write("timestamp \t voltage0 \t current0 \t voltage1 \t current1"+"\n")	
	print "timestamp \t voltage0 \t current0 \t voltage1 \t current1"+"\n"	

	while True:

		ts = getTimestamp()
		v0 = getVoltage("-rv1")
		i0 = getCurrent("-rc1")
		v1 = getVoltage("-rv0")
		i1 = getCurrent("-rc0")

		currentFile.write(str(ts)+","+str(v0)+","+str(i0)+","+str(v1)+","+str(i1)+","+"\n")
		print str(ts)+"\t"+str(v0)+"\t"+str(i0)+"\t"+str(v1)+"\t"+str(i1)+"\t"+"\n"
		if isTest:
			break
		else:
			time.sleep(1)

