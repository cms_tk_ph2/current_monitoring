import time, datetime, subprocess
from array import array

isTest = False

def getTimestamp():
	return str(time.time()).rstrip('\n')

def getVoltage():
	if isTest:
		commandV = ["echo","300"]
	else:
		commandV = ["python", "keithleyControl.py","-rv"]
	pipe = subprocess.Popen(commandV, stdout=subprocess.PIPE)
	text = pipe.communicate()
	return text[0].rstrip('\n')


def getCurrent():
	if isTest:
		commandV = ["echo","0.1"]
	else:
		commandV = ["python", "keithleyControl.py","-rc"]
	pipe = subprocess.Popen(commandV, stdout=subprocess.PIPE)
	text = pipe.communicate()
	return text[0].rstrip('\n')

def getTemperature():
	return "10"

def getHumidity():
	return "10"



if __name__=='__main__':

#def update_data_file(timestamp, temperature, humidity):

	environmentFile = open("environment.csv",'a')

	while True:

		ts = getTimestamp()
		v = getVoltage()
		i = getCurrent()
		temp = getTemperature()
		hum = getHumidity();

	
		environmentFile.write(str(ts)+","+str(v)+","+str(i)+","+str(temp)+","+str(hum)+"\n")				
		print str(ts)+","+str(v)+","+str(i)+","+str(temp)+","+str(hum)

		if isTest:
			break
		else:
			time.sleep(10)

