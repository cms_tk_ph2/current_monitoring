This manual explains how to set the voltages of the two Keithleys, run a script to collect the temperature,humidity and HV measurements and run a script to update these measurements on the web.

	1) Setting the voltage of the Keitleys	

		Run the following python script: "python keithleyControl.py -p /dev/ttyUSB0" to control the keitley in port /dev/ttyUSB0 interactively.
        If you want to change the voltages on the keithleys you should stop the env_monitoring.py script to prevent interference. To do this go to cmsuptracker002 type "tmux attach" and do ctrl+c to stop the script. After you are done restart the script by typing "python env_monitoring.py" in tmux (see step 2).
		All the options are explained on the screen, to set the voltage immediately to a certain value type that value and ENTER. If you want to ramp up or down the voltage slowly to X in steps of Y Volt type vs "ENTER" Y  "ENTER" X. 
 
		The other options are straightforward and are explained on the screen.

		To set the voltage in one command one can also type in a terminal:
			"python keithleyControl.py -p /dev/ttyUSB0 --v -600"
			To set the Keithley connected to /dev/ttyUSB0 to -600V.
		Or:
			"python keitleyControl.py -p /dev/ttyUSB0 --vs 50 -600"
			To set the Keithley connected to /dev/ttyUSB0 to -600V in steps of 50V.
            (Do not forget to restart env_monitoring.py script!)

		ONCE THE VOLTAGE IS SET EXIT THE PROGRAM WITH CTRL+C!!! DO NOT USE EXIT BECAUSE THE KEITLEYS WILL BE RESET!!!

	2) Running a script to write data to the environment measurement file

		A script is used to automatically write the voltage, current, temperature and humidity measured by the sensors to a file called "currents.csv". This script is called "env_monitoring.py". 
		Run it in the background with tmux (just like "python env_monitoring.py"). Another script will take care of creating the plots.

	3) Creating plots of the environment measurements

		The script makeplot.cc will make a png of the environment data plots. It is made into an executable with the Makefile in the directory and it is ran just like this: "./makeplot 10".
		The 10 in this example is the amount of minutes you would like to see in the past. The plots are automatically published online. "cmstkph2-beamtest.web.cern.ch/cmstkph2-beamtest/monitoring.html" 

		makeplot.cc is run as an acron job in the background all the time such that the website is always up to date within two minutes of the actual time. This is of course only true if currents.csv is kept up to date with env_monitoring.py (see 2) :) ).

		Type "acrontab -e" to see the acronjobs that are running.	
