makeplots: makeplot.cc data.C
	g++ -o makeplot `root-config --cflags` makeplot.cc data.C `root-config --glibs`
clean:
	rm -f *.o
